import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

const firebaseConfig = {
  apiKey: "AIzaSyAKlKMcAWR26SEVc1JZLN0m_SD2oYr6vew",
  authDomain: "landing-selector.firebaseapp.com",
  databaseURL: "https://landing-selector.firebaseio.com",
  projectId: "landing-selector",
  storageBucket: "landing-selector.appspot.com",
  messagingSenderId: "222738198732",
  appId: "1:222738198732:web:23504747859562f4"
};

firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from './Components/Landing/Landing';
import Ranking from './Components/Landing/Ranking';
import Login from './Components/Platform/Login';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: null
    }
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/Empresas" component={Ranking}/>
            <Route exact path="/Acceso" component={Login} />
          </Switch>
        </Router>
      </div>
    )
  }
  ;
}

export default App;

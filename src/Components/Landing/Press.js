import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBBtn, MDBAnimation, MDBMask } from "mdbreact";
import lunPic from "../../Images/lunPic.JPG";
import margaPic from "../../Images/margaPic.JPG";
import mostradorPic from "../../Images/mostradorPic.JPG";

const BlogPage = () => {
    return (
        <div id="press">

            <div className="pb-3 pt-3 blue-gradient">
                <b/>
                <h2 className="h1-responsive font-weight-bold my-5 white-text text-center align-center">Punto de Prensa</h2>
                {/* <p className="lead white-text w-responsive mx-auto mb-5 text-center">
                    Conoce más de Selector por los distintos reportajes en los que hemos aparecido.</p> */}
            </div>
            <MDBContainer className="my-5 px-5 pb-5">

                <MDBAnimation reveal type="fadeInLeft" duration="1s">
                    <MDBRow>
                        <MDBCol lg="5" xl="4">
                            <MDBView hover className="rounded z-depth-1-half mb-lg-0 mb-4">
                                <img
                                    width="100%"
                                    className="img-fluid"
                                    src={lunPic}
                                    alt=""
                                />
                                <a href="https://www.lun.com/Pages/NewsDetail.aspx?dt=2019-06-11&SupplementId=0&BodyID=0&PaginaId=23&r=w" rel="noopener noreferrer" target="_blank">
                                    <MDBMask overlay="white-slight" />
                                </a>
                            </MDBView>
                        </MDBCol>
                        <MDBCol lg="7" xl="8">
                            <h3 className="font-weight-bold mb-3 p-0 indigo-text">
                                <strong>Basurero inteligente separa los desechos reciclables de los
                                    que no lo son
                                </strong>
                            </h3>
                            <p className="dark-grey-text">
                                Acaban de obtener un fondo del concurso Talento Emprendedor 2 de la
                                Caja los Andes.</p>
                            <p>Por <a href="https://www.lun.com/Pages/NewsDetail.aspx?dt=2019-06-11&SupplementId=0&BodyID=0&PaginaId=23&r=w" className="font-weight-bold">LUN</a>, 11/06/2019</p>
                            <a href="https://www.lun.com/Pages/NewsDetail.aspx?dt=2019-06-11&SupplementId=0&BodyID=0&PaginaId=23&r=w" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                <MDBBtn color="primary" size="md">Leer más</MDBBtn>
                            </a>
                        </MDBCol>
                    </MDBRow>
                </MDBAnimation>

                <hr className="my-5" />
                <MDBAnimation reveal type="fadeInLeft" duration="1s">
                    <MDBRow>
                        <MDBCol lg="5" xl="4">
                            <MDBView hover className="rounded z-depth-1-half mb-lg-0 mb-4">
                                <img
                                    width="100%"
                                    className="img-fluid"
                                    src={mostradorPic}
                                    alt=""
                                />
                                <a href="https://margamargaonline.cl/regional/16146-proyecto-de-reciclaje-inteligente-de-duoc-uc-gana-fondo-de-inversion" rel="noopener noreferrer" target="_blank">
                                    <MDBMask overlay="white-slight" />
                                </a>
                            </MDBView>
                        </MDBCol>
                        <MDBCol lg="7" xl="8">
                            <h3 className="font-weight-bold mb-3 p-0 indigo-text">
                                <strong>Emprendedores que resuelven problemáticas sociales</strong>
                            </h3>
                            <p className="dark-grey-text">
                                Emprendedores que resuelven problemáticas
                                de dependencia, discapacidad y vivienda fueron premiados con
                            hasta con $25 millones.</p>
                            <p>Por <a href="https://m.elmostrador.cl/agenda-pais/2019/05/23/emprendedores-que-resuelven-problematicas-de-dependencia-discapacidad-y-vivienda-fueron-premiados-con-hasta-con-25-millones/" className="font-weight-bold"> Agenda País</a>, 23/05/2019 </p>
                            <a href="https://m.elmostrador.cl/agenda-pais/2019/05/23/emprendedores-que-resuelven-problematicas-de-dependencia-discapacidad-y-vivienda-fueron-premiados-con-hasta-con-25-millones/" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                <MDBBtn color="primary" size="md">Leer más</MDBBtn>
                            </a>
                        </MDBCol>
                    </MDBRow>
                </MDBAnimation>

                <hr className="my-5" />

                <MDBAnimation reveal type="fadeInLeft" duration="1s">
                    <MDBRow>
                        <MDBCol lg="5" xl="4">
                            <MDBView hover className="rounded z-depth-1-half mb-lg-0 mb-4">
                                <img
                                    width="100%"
                                    className="img-fluid"
                                    src={margaPic}
                                    alt=""
                                />
                                <a href="https://margamargaonline.cl/regional/16146-proyecto-de-reciclaje-inteligente-de-duoc-uc-gana-fondo-de-inversion" rel="noopener noreferrer" target="_blank">
                                    <MDBMask overlay="white-slight" />
                                </a>
                            </MDBView>
                        </MDBCol>
                        <MDBCol lg="7" xl="8">
                            <h3 className="font-weight-bold mb-3 p-0 indigo-text">
                                <strong>Proyecto de reciclaje inteligente gana fondo de inversión</strong>
                            </h3>
                            <p className="dark-grey-text">Gracias a la obtención de estos recursos, Selector
                            podrá desarrollar las primeras 100 unidades.</p>
                            <p>Por<a href="https://margamargaonline.cl/regional/16146-proyecto-de-reciclaje-inteligente-de-duoc-uc-gana-fondo-de-inversion" className="font-weight-bold"> Marga-Marga Online</a>, 23/05/2019</p>
                            <a href="https://margamargaonline.cl/regional/16146-proyecto-de-reciclaje-inteligente-de-duoc-uc-gana-fondo-de-inversion" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                <MDBBtn color="primary" size="md">Leer más</MDBBtn>
                            </a>
                        </MDBCol>
                    </MDBRow>
                </MDBAnimation>

            </MDBContainer>
        </div>
    );
}

export default BlogPage;
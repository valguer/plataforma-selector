import React, { Component } from 'react';
import { Pie } from "react-chartjs-2";
import { MDBContainer, MDBRow, MDBCard, MDBCardBody, MDBIcon, MDBCol, MDBView, MDBMask, MDBAnimation } from 'mdbreact';
import Nav from '../Landing/Nav';
import '../Landing/Ranking.css';


const empresas = [
    {
        id: 'empa',
        nombre: 'Empresa A',
        plastico: 25,
        vidrio: 8,
        alumnio: 6,
        web: 'www.empresaa.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
    {
        id: 'empb',
        nombre: 'Empresa B',
        plastico: 3,
        vidrio: 5,
        alumnio: 2,
        web: 'www.empresab.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
    {
        id: 'empc',
        nombre: 'Empresa C',
        plastico: 4,
        vidrio: 4,
        alumnio: 3,
        web: 'www.empresac.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
    {
        id: 'empx',
        nombre: 'Empresa X',
        plastico: 5,
        vidrio: 8,
        alumnio: 6,
        web: 'www.empresax.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
    {
        id: 'empy',
        nombre: 'Empresa Y',
        plastico: 3,
        vidrio: 5,
        alumnio: 2,
        web: 'www.empresay.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
    {
        id: 'empz',
        nombre: 'Empresa Z',
        plastico: 4,
        vidrio: 4,
        alumnio: 3,
        web: 'www.empresaz.cl',
        inicio: '01/08/2019',
        toneladas: 10,
        pic: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg'
    },
]


class Ranking extends Component {

    constructor(props) {
        super(props);

        this.state = {
            totalToneladasPlastico: 0,
            totalToneladasVidrio: 0,
            totalToneladasAluminio: 0,
            toneladasTotales: 0
        }
    }

    componentDidMount() {
        this.contadortotales(empresas);
    }

    contadortotales(data) {
        let pla = 0;
        let vid = 0;
        let alu = 0;
        let tot = 0;
        for (var ton in data) {
            pla += data[ton].plastico;
            vid += data[ton].vidrio;
            alu += data[ton].alumnio;
        }
        tot += pla;
        tot += vid;
        tot += alu;
        this.setState({
            totalToneladasPlastico: pla,
            totalToneladasVidrio: vid,
            totalToneladasAluminio: alu,
            toneladasTotales: tot
        })
    }

    render() {
        // const { empresas } = this.props;
        return (
            <div>
                <Nav />
                <div id="ranking" className="text-center">
                    <MDBView>
                        <MDBMask id="mask" className="mask rgba-gradient d-flex justify-content-center align-items-center gradient">
                            <MDBRow>
                                <MDBCol>
                                    <div>
                                        <MDBAnimation type="fadeInDown" duration="1s">
                                            <MDBContainer>
                                                <h1 className="h1-responsive font-weight-bold white-text">Top Empresas</h1>
                                                <hr className="hr-light" />
                                                <div className="lead white-text w-responsive mx-auto mb-5">Éstas son las empresas que
                                                    junto a Selector, han asumido la responsabilidad social de contribuir al cuidado del medio ambiente. Así, ya han
                                                reciclado más de <strong>{this.state.toneladasTotales} toneladas </strong>.
                                                        ¿Qué esperas para sumarte?
                                            </div>
                                                {/* <div style={{ width: '22rem', marginLeft: 'auto', marginRight: 'auto', }}>
                                                <Pie
                                                    data={{
                                                        labels: ["Plástico", "Vidrio", "Alumnio"],
                                                        datasets: [
                                                            {
                                                                data: [this.state.totalToneladasPlastico, this.state.totalToneladasVidrio, this.state.totalToneladasAluminio],
                                                                backgroundColor: [
                                                                    "#673dff",
                                                                    "#9265ff",
                                                                    "#e8d8ff"
                                                                ],
                                                                hoverBackgroundColor: [
                                                                    "#673dff",
                                                                    "#9265ff",
                                                                    "#e8d8ff"
                                                                ]
                                                            }
                                                        ]
                                                    }}
                                                    options={{ responsive: true }} />
                                            </div> */}
                                            </MDBContainer>
                                        </MDBAnimation>
                                    </div>
                                </MDBCol>
                            </MDBRow>
                        </MDBMask>
                    </MDBView>
                    <MDBContainer >
                        <MDBRow className="d-flex  d-flex justify-content-center">
                            {
                                empresas.map(e => {
                                    return (
                                        <MDBAnimation reveal type="fadeInLeft" duration="2s">
                                            <MDBCard style={{ width: "21rem", margin: '20px' }} >
                                                <img style={{ borderTopLeftRadius: '5px', borderTopRightRadius: '5px', width: '100%' }} src={e.pic} alt={e.nombre} />
                                                <div style={{ marginTop: '-3rem', marginBottom: '2rem', color: 'white', textShadow: '2px 2px 4px #000000' }} key={e.id} className="d-flex w-100 justify-content-between">
                                                    <h5 style={{ marginLeft: '1rem' }} className="mb-1 font-weight-bold">{e.nombre}</h5>
                                                    <span>
                                                        <a href={e.web} rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                                            <MDBIcon style={{ color: 'white', textShadow: '2px 2px 4px #000000' }} icon="angle-double-right" />
                                                        </a>
                                                    </span>
                                                </div>
                                                <MDBCardBody>
                                                    <div className="mb-1">
                                                        <Pie
                                                            data={{
                                                                labels: ["Plástico", "Vidrio", "Alumnio"],
                                                                datasets: [
                                                                    {
                                                                        data: [e.plastico, e.vidrio, e.alumnio],
                                                                        backgroundColor: [
                                                                            "#673dff",
                                                                            "#9265ff",
                                                                            "#e8d8ff"
                                                                        ],
                                                                        hoverBackgroundColor: [
                                                                            "#673dff",
                                                                            "#9265ff",
                                                                            "#e8d8ff"
                                                                        ]
                                                                    }
                                                                ]
                                                            }}
                                                            options={{ responsive: true, animation: true }} />
                                                    </div>
                                                    <hr />
                                                    <small className="indigo-text">{e.toneladas} toneladas desde el {e.inicio}</small>
                                                </MDBCardBody>
                                            </MDBCard>
                                        </MDBAnimation>
                                    )
                                })
                            }
                        </MDBRow>
                    </MDBContainer>
                </div>
            </div>
        )
    };
}

export default Ranking;
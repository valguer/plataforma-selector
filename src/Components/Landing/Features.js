import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBIcon, MDBCard, MDBCardBody, MDBCardTitle, MDBAnimation } from "mdbreact";

const Features = () => {
    return (
        <section id="servicio" className="text-center my-5">
            <MDBContainer>
                <h2 className="h1-responsive font-weight-bold my-5 indigo-text">¿Qué ofrecemos?</h2>
                <p className="lead grey-text w-responsive mx-auto mb-5">
                    Un servicio completo de separación, reciclaje y reportería de todos la basura que pase por nuestros
                    contenedores inteligentes. Además, si eres una empresa afecta a la ley REP, te invitamos especialmente
                    a contactarnos. Así que recuerda:</p>

                <MDBRow className="d-flex  d-flex justify-content-center">
                    <MDBAnimation reveal type="fadeInUp" duration="1s">
                        <MDBCol md="4">

                            <MDBCard style={{ width: "17rem", margin: '10px' }} >
                                <div style={{ borderRadius: '5px', width: '100%', height: '150px' }} className="primary-color-dark">
                                    <MDBIcon style={{ paddingTop: '25px' }} icon="robot" size="3x" className="white-text" />
                                    <MDBCardTitle className="font-weight-bold my-4 white-text">Separamos</MDBCardTitle>
                                </div>
                                <MDBCardBody>
                                    <p className="mb-md-0 mb-5 indigo-text">
                                        Nuestra suite SELECTOR, con inteligencia artificial, separa automáticamente
                                        toda la basura con potencial de reciclaje y lleva control de los materiales procesados.</p>
                                </MDBCardBody>
                            </MDBCard>

                        </MDBCol>
                    </MDBAnimation>
                    <MDBAnimation reveal type="fadeInUp" duration="2s">
                        <MDBCol md="4">

                            <MDBCard style={{ width: "17rem", margin: '10px' }} >
                                <div style={{ borderRadius: '5px', width: '100%', height: '150px' }} className="primary-color-dark">
                                    <MDBIcon style={{ paddingTop: '25px' }} icon="recycle" size="3x" className="white-text" />
                                    <MDBCardTitle className="font-weight-bold my-4 white-text">Reciclamos</MDBCardTitle>
                                </div>
                                <MDBCardBody>
                                    <p className="mb-md-0 mb-5 indigo-text">
                                        Una vez reunida una cantidad suficiente, el sistema notifica a nuestra red de recicladores para hacer
                                        el retiro de todo lo que separamos a un centro de reciclaje acreditado.</p>
                                </MDBCardBody>
                            </MDBCard>

                        </MDBCol>
                    </MDBAnimation>
                    <MDBAnimation reveal type="fadeInUp" duration="2s">
                        <MDBCol md="4">

                            <MDBCard style={{ width: "17rem" }} >
                                <div style={{ borderRadius: '5px', width: '100%', height: '150px' }} className="primary-color-dark">
                                    <MDBIcon style={{ paddingTop: '25px' }} icon="chart-line" size="3x" className="white-text" />
                                    <MDBCardTitle className="font-weight-bold my-4 white-text">Reportamos</MDBCardTitle>
                                </div>
                                <MDBCardBody>
                                    <p className="mb-md-0 mb-5 indigo-text">
                                        Tendrás acceso a nuestra plataforma para  revisar en tiempo real las toneladas que
                                    estamos reciclado para ti. Además, emitiremos un certificado mensual de todo lo procesado.</p>
                                </MDBCardBody>
                            </MDBCard>

                        </MDBCol>
                    </MDBAnimation>
                </MDBRow>
            </MDBContainer>
            <br />
        </section>
    );
}

export default Features;
import React, { Component } from "react";
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBIcon, MDBContainer } from "mdbreact";
import { Link, animateScroll as scroll } from "react-scroll";
import logo from "../../Images/logo.png";

class Nav extends Component {
    state = {
        collapsed: false
    };

    handleTogglerClick = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    };

    scrollToTop = () => {
        scroll.scrollToTop();
    };

    render() {
        const overlay = (
            <div
                id="sidenav-overlay"
                style={{ backgroundColor: "transparent" }}
                onClick={this.handleTogglerClick}
            />
        );
        return (
            <div id="nav">
                <div>
                    <header id="inicio">
                        <MDBNavbar
                            className="blue-gradient"
                            dark
                            expand="md"
                            fixed="top"
                            scrolling
                            transparent
                        >
                            <MDBContainer>
                                <MDBNavbarBrand onClick={this.scrollToTop} href="/">
                                    <img alt="SELECTOR" src={logo} width="35px" style={{ marginRight: "10px" }} /> <strong>SELECTORCORP</strong>
                                </MDBNavbarBrand>
                                <MDBNavbarToggler onClick={this.handleTogglerClick} />
                                <MDBCollapse isOpen={this.state.collapsed} navbar>
                                    <MDBNavbarNav left>
                                        <MDBNavItem>
                                            <MDBNavLink to="" onClick={this.scrollToTop}>
                                                Inicio
                                            </MDBNavLink>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBNavLink to="">
                                                <Link
                                                    to="servicio" spy={true}
                                                    smooth={true}
                                                    offset={-100}
                                                    duration={500}>
                                                    Servicio
                                                </Link>
                                            </MDBNavLink>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBNavLink to="">
                                                <Link
                                                    to="press" spy={true}
                                                    smooth={true}
                                                    offset={-150}
                                                    duration={500}>
                                                    Prensa
                                                </Link>
                                            </MDBNavLink>
                                        </MDBNavItem>
                                        <MDBNavItem>
                                            <MDBNavLink to="">
                                                <Link
                                                    to="contact" spy={true}
                                                    smooth={true}
                                                    offset={-60}
                                                    duration={500}>
                                                    Hablemos
                                                </Link>
                                            </MDBNavLink>
                                        </MDBNavItem>
                                        {/* <MDBNavItem>
                                            <MDBNavLink to="/Empresas" onClick={this.scrollToTop}>Top Empresas</MDBNavLink>
                                        </MDBNavItem> */}
                                        <MDBNavItem>
                                            <MDBNavLink to="/Acceso" onClick={this.scrollToTop}>Acceso</MDBNavLink>
                                        </MDBNavItem>
                                    </MDBNavbarNav>
                                    <MDBNavbarNav right>
                                        <div className="text-center d-flex justify-content-center white-label">
                                            <a href="https://m.facebook.com/selectorchile/" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                                <MDBIcon fab icon="facebook-f" className="white-text" />
                                            </a>
                                            <a href="https://twitter.com/chileselector" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                                <MDBIcon fab icon="twitter" className="white-text" />
                                            </a>
                                            <a href="https://www.instagram.com/selectorchile/?hl=es" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                                <MDBIcon fab icon="instagram" className="white-text" />
                                            </a>
                                        </div>
                                    </MDBNavbarNav>
                                </MDBCollapse>
                            </MDBContainer>
                        </MDBNavbar>
                        {this.state.collapsed && overlay}
                    </header>
                </div>
            </div>
        );
    }
}

export default Nav;
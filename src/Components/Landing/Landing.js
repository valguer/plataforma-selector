import React, { Component } from 'react';
import Nav from '../Landing/Nav';
import Intro from '../Landing/Intro';
import Features from '../Landing/Features';
import Press from '../Landing/Press';
import Contact from '../Landing/Contact';
import Footer from '../Landing/Footer';



class Landing extends Component {
    render() {
        return (
            <div>
                <Nav />
                <Intro />
                <Features />
                <Press />
                <Contact />
                <Footer />
            </div>
        )
    };
}

export default Landing;

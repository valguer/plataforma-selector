import React, { Component } from "react";
import { MDBAnimation, MDBMask, MDBRow, MDBCol, MDBBtn, MDBView, MDBContainer } from "mdbreact";
import { Link } from "react-scroll";
import "../Landing/Intro.css";
import proceso from "../../Images/proceso.gif";


class Intro extends Component {
    ;
    render() {
        return (
            <div id="apppage">
                <MDBView>
                    <MDBMask id="mask" className="mask rgba-gradient d-flex justify-content-center align-items-center gradient">
                        <MDBContainer>
                            <MDBRow>
                                <div className="white-text text-center text-md-left col-md-6 mt-xl-5 mb-5">

                                    <MDBAnimation type="fadeInLeft" duration="1s">
                                        <h1 className="h1-responsive font-weight-bold mt-sm-5">
                                            Inteligencia Artificial al Servicio del Reciclaje{" "}
                                        </h1>
                                        <hr className="hr-light" />
                                        <h6 className="mb-4">
                                            Selector es una nueva y modernizada forma de reciclar, donde la separación de residuos se hace
                                            in situ, según lo indica la ley REP 20.920. Todo esto es posible gracias al poder de una red inteligente de mecanismos, que nos permiten
                                            clasificar automáticamente los desperdicios y así, aumentar el volumen de reciclaje actual.</h6>

                                        <Link
                                            to="contact" spy={true}
                                            smooth={true}
                                            offset={-60}
                                            duration={500}>
                                            <MDBBtn color="white">Hablemos</MDBBtn>
                                        </Link>
                                        <a href="https://www.leychile.cl/Navegar?idNorma=1090894" rel="noopener noreferrer" target="_blank">
                                            <MDBBtn outline color="white">Ley REP</MDBBtn>
                                        </a>
                                    </MDBAnimation>
                                </div>
                                <MDBCol md="6" xl="5" className="mt-xl-5">
                                    <MDBAnimation type="fadeInRight" duration="1s" delay="1s">
                                        <img
                                            id="proceso"
                                            style={{right: '-50px', top: '50px'}}
                                            width="90%"
                                            src={proceso}
                                            alt="Proceso Selector"
                                            className="img-fluid"
                                        />
                                    </MDBAnimation>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                    </MDBMask>
                </MDBView>
            </div>
        );
    }
}

export default Intro;
import React from "react";
import { MDBContainer, MDBFooter, MDBIcon } from "mdbreact";

const Footer = () => {
    return (
        <MDBFooter className="font-small">
            <div className="footer-copyright text-center py-3 blue-gradient">
                <MDBContainer fluid>
                    &copy; {new Date().getFullYear()} Copyright: <a href="https://www.selectorchile.com"> selectorchile.com </a>
                </MDBContainer>
            </div>
            <div className="text-center d-flex justify-content-center white-label primary-color-dark">
                <a href="https://m.facebook.com/selectorchile/" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                    <MDBIcon fab icon="facebook-f" className="white-text" />
                </a>
                <a href="https://twitter.com/chileselector" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                    <MDBIcon fab icon="twitter" className="white-text" />
                </a>
                <a href="https://www.instagram.com/selectorchile/?hl=es" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                    <MDBIcon fab icon="instagram" className="white-text" />
                </a>
            </div>
        </MDBFooter>
    );
}

export default Footer;
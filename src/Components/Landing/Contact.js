import React, { Component } from 'react';
import { MDBMask, MDBRow, MDBCol, MDBView, MDBContainer, MDBAnimation } from "mdbreact";
import '../Platform/Login.css'
import '../Landing/Contact.css'

class Contact extends Component {
    render() {
        return (
            <div id="contact">
                <MDBView>
                    <MDBMask className="mask rgba-gradient d-flex justify-content-center gradient">
                        <MDBContainer>
                            <MDBRow>
                                <MDBCol className="my-5 mt-xl-5 mb-5">
                                    <MDBAnimation reveal type="fadeInUpBig" duration="2s">
                                        <iframe
                                            title="frm"
                                            src="https://docs.google.com/forms/d/e/1FAIpQLSfmL4UUC4yKoYt0vUSXTtLQrtTmJBAgiFmwOKhQFmUnNhPxhg/viewform?embedded=true"
                                            width="100%"
                                            height="1300px"
                                            frameborder="0"
                                            marginheight="0"
                                            marginwidth="0">
                                            Cargando…
                                            </iframe>
                                    </MDBAnimation>
                                </MDBCol>
                                <br />
                                <br />
                            </MDBRow>
                        </MDBContainer>
                    </MDBMask>
                </MDBView>

            </div>

        )
    };
}

export default Contact;
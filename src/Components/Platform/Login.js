import React, { Component } from 'react';
import {
    MDBIcon,
    MDBCardText,
    MDBAnimation,
    MDBMask,
    MDBRow,
    MDBCol,
    MDBBtn,
    MDBView,
    MDBContainer,
    MDBInput,
    MDBCard,
    MDBCardBody,
    MDBFooter,
    MDBProgress,
    MDBListGroupItem,
    MDBListGroup,
    MDBBadge
} from "mdbreact";
import { Pie } from "react-chartjs-2";
import Nav from '../Landing/Nav'
import Footer from '../Landing/Footer'
import '../Platform/Login.css'
import * as firebase from 'firebase';


class Landing extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            email: '',
            password: '',
            nivelPlastico: 0,
            nivelMetal: 0,
            nivelVidrio: 0,
            nivelOtros: 0,
            cantidadPlastico: 0,
            cantidadMetal: 0,
            cantidadVidrio: 0,
            cantidadOtros: 0,
            total: 0
        }
        this.login = this.login.bind(this);
        this.signout = this.signout.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.authListener();
        this.actualizador();
    }

    actualizador() {
        const docNivel = firebase.firestore().collection('selector').doc('nivel');
        docNivel.onSnapshot(snap => {
            this.setState({
                nivelPlastico: snap.data().plastico,
                nivelMetal: snap.data().metal,
                nivelVidrio: snap.data().vidrio,
                nivelOtros: snap.data().otros
            })
        });

        const docCanti = firebase.firestore().collection('selector').doc('cantidad');
        docCanti.onSnapshot(snap => {
            this.setState({
                cantidadPlastico: snap.data().plastico,
                cantidadMetal: snap.data().metal,
                cantidadVidrio: snap.data().vidrio,
<<<<<<< HEAD
                cantidadOtros: snap.data().otros,
                total: snap.data().plastico + snap.data().metal + snap.data().vidrio
            })
        });

        /* const docTotal = firebase.firestore().collection('selector').doc('total');
=======
                cantidadOtros: snap.data().otros
            })
        });

        const docTotal = firebase.firestore().collection('selector').doc('total');
>>>>>>> master
        docTotal.onSnapshot(snap => {
            this.setState({
                total: snap.data().total
            })
<<<<<<< HEAD
        }); */
=======
        });
>>>>>>> master
    }

    authListener() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ user });
            } else {
                this.setState({ user: null });
            }
        });
    }

    login(e) {
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password);
    }

    signout() {
        firebase.auth().signOut();
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }


    render() {

        const dataPie = {
            labels: ['Plástico', 'Vidrio', 'Aluminio'],
            datasets: [
                {
                    data: [this.state.cantidadPlastico, this.state.cantidadVidrio, this.state.cantidadMetal],
                    backgroundColor: ['#ffbb33', '#00C851', '#607d8b'],
                }
            ]
        }
        return (
            <div>
                {
                    this.state.user ? (
                        <div>
                            <Nav />
                            <div id="apppage">
                                <MDBView>
                                    <MDBMask id="mask" className="mask rgba-gradient justify-content-center gradient">
                                        <MDBContainer style={{ marginTop: '7rem' }}>
                                            <MDBAnimation type="fadeInLeft" duration="1s">
                                                <MDBRow>
                                                    <MDBCol xl="6" lg="6" md="6" sm="12" xs="12">
                                                        <MDBRow>
                                                            <MDBCol xl="6" lg="6" md="12" sm="12" xs="12">
                                                                <MDBCard className="mb-4" color="warning-color-dark">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="wine-bottle" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL PLÁSTICO</p>
<<<<<<< HEAD
                                                                        <MDBProgress min={0} max={100} color="warning" animated value={this.state.cantidadPlastico} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.cantidadPlastico} % de llenado</MDBCardText>
=======
                                                                        <MDBProgress min={0} max={100} color="warning" animated value={this.state.nivelPlastico} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.nivelPlastico} % de llenado</MDBCardText>
>>>>>>> master
                                                                    </MDBCardBody>
                                                                </MDBCard>

                                                                <MDBCard className="mb-4" color="success-color-dark">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="wine-glass" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL VIDRIO</p>
<<<<<<< HEAD
                                                                        <MDBProgress min={0} max={100} color="success" animated value={this.state.cantidadVidrio} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.cantidadVidrio} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>

                                                            <MDBCol xl="6" lg="6" md="12" sm="12" xs="12">

                                                                <MDBCard className="mb-4" color="blue-grey">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="robot" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL ALUMINIO</p>
                                                                        <MDBProgress min={0} max={100} color="info" animated value={this.state.cantidadMetal} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.cantidadMetal} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>

                                                                <MDBCard className="mb-4" color="danger-color-dark">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="times-circle" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL OTROS</p>
                                                                        <MDBProgress min={0} max={100} color="danger" animated value={this.state.cantidadOtros} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.cantidadOtros} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>
                                                        </MDBRow>
                                                    </MDBCol>
                                                    <MDBCol xl="6" lg="6" md="6" sm="12" xs="12">
                                                        <MDBRow>
                                                            <MDBCol md="12" className="mb-4">
                                                                <MDBCard className="mb-4">
                                                                    <MDBCardBody>
                                                                        <Pie data={dataPie} height={160} options={{ responsive: true }} />
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>
                                                        </MDBRow>
                                                    </MDBCol>
                                                </MDBRow>
                                            </MDBAnimation>

                                            <MDBAnimation type="fadeInUp" duration="1s">
                                                <MDBRow xl="12" lg="12" md="12" sm="12" xs="12">
                                                    <MDBCol className="mb-4">
                                                        <MDBCard>
                                                            <MDBCardBody>
                                                                <MDBListGroup className="list-group-flush">
                                                                    <h1>
                                                                        <MDBBadge color="warning-color" className="float-right">{this.state.cantidadPlastico} tons<MDBIcon icon="wine-bottle" className="ml-4" /></MDBBadge>
                                                                    </h1>
                                                                </MDBListGroup>
                                                            </MDBCardBody>
                                                        </MDBCard>
                                                    </MDBCol>
                                                    <MDBCol md="3" className="mb-4">
                                                        <MDBCard color="success">
                                                            <MDBCardBody>
                                                                <MDBListGroup className="list-group-flush">
                                                                    <h1>
                                                                        <MDBBadge color="success-color" className="float-right">{this.state.cantidadVidrio} tons<MDBIcon icon="wine-glass" className="ml-4" /></MDBBadge>
                                                                    </h1>
                                                                </MDBListGroup>
                                                            </MDBCardBody>
                                                        </MDBCard>
                                                    </MDBCol>
                                                    <MDBCol md="3" className="mb-4">
                                                        <MDBCard>
                                                            <MDBCardBody>
                                                                <MDBListGroup className="list-group-flush">
                                                                    <h1>
                                                                        <MDBBadge color="blue-grey" className="float-right">{this.state.cantidadMetal} tons<MDBIcon icon="robot" className="ml-4" /></MDBBadge>
                                                                    </h1>
                                                                </MDBListGroup>
                                                            </MDBCardBody>
                                                        </MDBCard>
                                                    </MDBCol>
                                                    <MDBCol md="3" className="mb-4">
                                                        <MDBCard>
                                                            <MDBCardBody>
                                                                <MDBListGroup className="list-group-flush">
                                                                    <h1>
                                                                        <MDBBadge color="primary-color" className="float-right">{this.state.total} tons<MDBIcon icon="heart" className="ml-4" /></MDBBadge>
                                                                    </h1>
                                                                </MDBListGroup>
                                                            </MDBCardBody>
                                                        </MDBCard>
                                                    </MDBCol>
                                                </MDBRow>
                                            </MDBAnimation>
=======
                                                                        <MDBProgress min={0} max={100} color="success" animated value={this.state.nivelVidrio} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.nivelVidrio} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>

                                                            <MDBCol xl="6" lg="6" md="12" sm="12" xs="12">

                                                                <MDBCard className="mb-4" color="blue-grey">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="robot" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL ALUMINIO</p>
                                                                        <MDBProgress min={0} max={100} color="info" animated value={this.state.nivelMetal} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.nivelMetal} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>

                                                                <MDBCard className="mb-4" color="danger-color-dark">
                                                                    <MDBCardBody>
                                                                        <div className="float-right">
                                                                            <MDBIcon className="white-text" size="2x" icon="times-circle" />
                                                                        </div>
                                                                        <p className="white-text">NIVEL REPROCESO</p>
                                                                        <MDBProgress min={0} max={100} color="danger" animated value={this.state.nivelOtros} className="mb-3" />
                                                                        <MDBCardText className="white-text">{this.state.nivelOtros} % de llenado</MDBCardText>
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>
                                                        </MDBRow>
                                                    </MDBCol>
                                                    <MDBCol xl="6" lg="6" md="6" sm="12" xs="12">
                                                        <MDBRow>
                                                            <MDBCol md="12" className="mb-4">
                                                                <MDBCard className="mb-4">
                                                                    <MDBCardBody>
                                                                        <Pie data={dataPie} height={160} options={{ responsive: true }} />
                                                                    </MDBCardBody>
                                                                </MDBCard>
                                                            </MDBCol>
                                                        </MDBRow>
                                                    </MDBCol>
                                                </MDBRow>
                                            </MDBAnimation>

                                            <MDBRow xl="12" lg="12" md="12" sm="12" xs="12">
                                                <MDBCol md="3" className="mb-4">
                                                    <MDBCard>
                                                        <MDBCardBody>
                                                            <MDBListGroup className="list-group-flush">
                                                                <MDBListGroupItem> Plástico
                                                                    <MDBBadge color="warning-color" pill className="float-right">{this.state.cantidadPlastico} tons<MDBIcon icon="wine-bottle" className="ml-1" />
                                                                    </MDBBadge>
                                                                </MDBListGroupItem>
                                                            </MDBListGroup>
                                                        </MDBCardBody>
                                                    </MDBCard>
                                                </MDBCol>
                                                <MDBCol md="3" className="mb-4">
                                                    <MDBCard>
                                                        <MDBCardBody>
                                                            <MDBListGroup className="list-group-flush">
                                                                <MDBListGroupItem> Vidrio
                                                                    <MDBBadge color="success-color" pill className="float-right">{this.state.cantidadVidrio} tons<MDBIcon icon="wine-glass" className="ml-1" />
                                                                    </MDBBadge>
                                                                </MDBListGroupItem>
                                                            </MDBListGroup>
                                                        </MDBCardBody>
                                                    </MDBCard>
                                                </MDBCol>
                                                <MDBCol md="3" className="mb-4">
                                                    <MDBCard>
                                                        <MDBCardBody>
                                                            <MDBListGroup className="list-group-flush">
                                                                <MDBListGroupItem> Aluminio
                                                                    <MDBBadge color="blue-grey" pill className="float-right">{this.state.cantidadMetal} tons<MDBIcon icon="robot" className="ml-1" />
                                                                    </MDBBadge>
                                                                </MDBListGroupItem>
                                                            </MDBListGroup>
                                                        </MDBCardBody>
                                                    </MDBCard>
                                                </MDBCol>
                                                <MDBCol md="3" className="mb-4">
                                                    <MDBCard>
                                                        <MDBCardBody>
                                                            <MDBListGroup className="list-group-flush">
                                                                <MDBListGroupItem> Total
                                                                    <MDBBadge color="primary-color" pill className="float-right">{this.state.total} tons<MDBIcon icon="times-circle" className="ml-1" />
                                                                    </MDBBadge>
                                                                </MDBListGroupItem>
                                                            </MDBListGroup>
                                                        </MDBCardBody>
                                                    </MDBCard>
                                                </MDBCol>
                                            </MDBRow>
>>>>>>> master
                                        </MDBContainer>
                                    </MDBMask>
                                </MDBView>
                            </div>
                            <MDBFooter className="font-small">
                                <div className="footer-copyright text-center py-3 blue-gradient">
                                    <MDBContainer fluid style={{ cursor: 'pointer' }}>
                                        <span onClick={this.signout}> Cerrar Sesión </span>
                                    </MDBContainer>
                                </div>
                                <div className="text-center d-flex justify-content-center white-label primary-color-dark">
                                    <a href="https://m.facebook.com/selectorchile/" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                        <MDBIcon fab icon="facebook-f" className="white-text" />
                                    </a>
                                    <a href="https://twitter.com/chileselector" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                        <MDBIcon fab icon="twitter" className="white-text" />
                                    </a>
                                    <a href="https://www.instagram.com/selectorchile/?hl=es" rel="noopener noreferrer" target="_blank" className="p-2 m-2">
                                        <MDBIcon fab icon="instagram" className="white-text" />
                                    </a>
                                </div>
                            </MDBFooter>
                        </div>
                    ) : (
                            <div>
                                <Nav />
                                <div id="apppage">
                                    <MDBView>
                                        <MDBMask id="mask" className="mask rgba-gradient d-flex justify-content-center align-items-center gradient">
                                            <MDBContainer>
                                                <MDBRow>
                                                    <div id="textoLogin" className="white-text text-center text-md-left col-md-6 mt-xl-5 mb-5">
                                                        <MDBAnimation type="fadeInLeft" duration="1s">
                                                            <h2 className="h1-responsive font-weight-bold mt-sm-5">
                                                                ACCESO A LA PLATAFORMA{" "}
                                                            </h2>
                                                            <hr className="hr-light" />
                                                            <h6 className="mb-4">
                                                                Selecciona el perfil que te fue asignado y completa tus datos para poder entrar.
                                                Si tienes problemas comunícate con nosotros.</h6>
                                                        </MDBAnimation>
                                                    </div>
                                                    <MDBCol md="6" xl="5" className="mt-xl-5">
                                                        <MDBAnimation type="fadeInRight" duration="1s" delay="1s">
                                                            <div id="classicformpage">
                                                                <MDBCol>
                                                                    <MDBCard id="classic-card">
                                                                        <MDBCardBody className="z-depth-2 white-text">
                                                                            {/* <MDBInput name="usuario" label="Tipo usuario" icon="user" /> */}
                                                                            <MDBInput
                                                                                name="email"
                                                                                label="Correo"
                                                                                icon="envelope"
                                                                                onChange={this.handleChange}
                                                                                className="white-text"
                                                                            />
                                                                            <MDBInput
                                                                                name="password"
                                                                                label="Contraseña"
                                                                                icon="lock"
                                                                                type="password"
                                                                                onChange={this.handleChange}
                                                                                className="white-text"
                                                                            />
                                                                            <div className="text-center mt-4 black-text">
                                                                                <MDBBtn color="indigo" onClick={this.login}>Acceder</MDBBtn>
                                                                            </div>
                                                                        </MDBCardBody>
                                                                    </MDBCard>
                                                                </MDBCol>
                                                            </div>
                                                        </MDBAnimation>
                                                    </MDBCol>
                                                </MDBRow>
                                            </MDBContainer>
                                        </MDBMask>
                                    </MDBView>
                                </div>
                                <Footer />
                            </div>
                        )
                }
            </div>
        )
    };
}

export default Landing;